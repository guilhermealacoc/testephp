<?php

include_once '../model/conexao.class.php';
include_once '../model/manager.class.php';

$manager = new Manager();

$data = $_POST;

if(isset($data) && !empty($data)){
    $manager->inserirDados("contato", $data);
    header("Location: ../view/crudsucesso.php?cadastro_com_sucesso");
}

?>