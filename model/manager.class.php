<?php

    class Manager extends Conexao{

        public function inserirDados($table, $data){
            $pdo = parent::get_instance();
            $fields = implode(", ", array_keys($data));
            $values = ":".implode(", :", array_keys($data));
            $sql = "INSERT INTO $table ($fields) VALUES ($values)";
            $statement = $pdo->prepare($sql);
            foreach($data as $key => $value){
                $statement->bindValue(":$key",$value, PDO::PARAM_STR);
            }
            $statement->execute();
        }

        public function listarDados($table) {
            $pdo = parent::get_instance();
            $sql = "SELECT * FROM $table order by codigo desc";
            $statement = $pdo->query($sql);
            $statement->execute();
            return $statement->fetchAll();
        }

        public function excluirDados($table, $codigo){
            $pdo = parent::get_instance();
            $sql = "delete from $table where codigo = :codigo";
            $statement = $pdo->prepare($sql);
            $statement->bindValue(":codigo", $codigo);
            $statement->execute();
        }

        public function pegaInfo($table, $codigo) {
            $pdo = parent::get_instance();
            $sql = "SELECT * FROM $table WHERE codigo = :codigo";
            $statement = $pdo->prepare($sql);
            $statement->bindValue(":codigo", $codigo);
            $statement->execute();
    
            return $statement->fetchAll();
        }

        public function atualizaContato($table, $data, $codigo) {
            $pdo = parent::get_instance();
            $new_values = "";
            foreach($data as $key => $value) {
                $new_values .= "$key=:$key, ";
            }
            $new_values = substr($new_values, 0, -2);
            $sql = "UPDATE $table SET $new_values WHERE codigo = :codigo";
            $statement = $pdo->prepare($sql);
            foreach($data as $key => $value) {
                $statement->bindValue(":$key", $value, PDO::PARAM_STR);
            }
            $statement->execute();
        }

    }

?>