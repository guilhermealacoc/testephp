<html lang="pt">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Teste PHP</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.1/examples/cover/">

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/estilo.css" rel="stylesheet">
  </head>

  <body class="text-center">

    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
    <?php
	      include_once('../menu.php');
    ?>

      <div class="container text-center">

        <h1 id="titulofoco" class="text-center display-4">Cadastre-se na agenda de contatos</h1>

        <form method="POST" action="../controller/inserircontato.php">
	
        <div id="agendatelefonica">
        
        <div class="form-group">
		
		<div class="form-group">
			Nome: <i class="fa fa-user"></i>
			<input class="form-control" type="text" name="nome" required autofocus><br>
		</div>

		<div class="form-group">
			E-mail: <i class="fa fa-envelope"></i>
			<input class="form-control" type="email" name="email" required><br>
		</div>

		<div class="form-group">
			Telefone: <i class="fab fa-whatsapp"></i>
			<input class="form-control" type="text" name="celular" required id="celular"><br>
		</div>
			
			<button class="btn btn-primary btn-lg">
				
				Cadastrar <i class="fa fa-user-plus"></i>

			</button><br><br>

		</div>

	</div>
</div>

</form>
             
      </div>
      

      <footer class="mastfoot mt-auto">
        
      </footer>
    </div>

    <script src="js/script.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script> 
    <script type="text/javascript">
	$(document).ready(function(){
		$("#celular").mask("(00) 00000-0000");
	});
</script>
  </body>
</html>