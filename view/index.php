<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.1/assets/img/favicons/favicon.ico">

    <title>Teste PHP</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.1/examples/cover/">

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/estilo.css" rel="stylesheet">
  </head>

  <body class="text-center">

    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
    <?php
	      include_once('menu.php');
    ?>

      <div class="container text-center">
        <div id="apresentacao"class="text-center">
          <h1 id="meunome" class="text-left">Guilherme de Alacoc Aquino.</h1>
          <p class="text-justify">Guilherme de Alacoc Aquino, 19 anos, estudante de Sistemas para Internet na Fatec São Roque. 
            Aficionado por tecnologia, ciência e literatura, Iniciei no mundo da internet com 10 anos, quando escrevia o Blog do Gui, usando CMS do Google e flash para o layout. 
            Técnico em Informática pela Etec Fernando Prestes, desenvolvi maiores habilidades em Banco de Dados e Desenvolvimento Web.</p>
        </div>

        <div id="perguntas"class="text-center">
            <h1 class="text-center display-4">PERGUNTAS</h1>
        
            <p class="text-justify perguntaquestionario">Pontue sua experiência de 0 (não possuo) à 5 (sou um jedi) para os itens:</p>
            
            <div class="text-center nivelconhecimento">
              <li class="text-justify" >CSS</li>
                <p class="text-left">
                  Projetos acadêmicos e técnicos envolvendo CSS3.
                </p>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="3"
                    aria-valuemin="0" aria-valuemax="5" style="width:30%">
                      3
                    </div>
                  </div> 
            </div>
            
            <div class="text-center nivelconhecimento">
              <li class="text-justify" >JavaScript</li>
                <p class="text-left">
                    Projetos acadêmicos utilizando OO e AJAX.
                </p>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="3"
                    aria-valuemin="0" aria-valuemax="5" style="width:30%">
                      3
                    </div>
                  </div> 
            </div>
            
            <div class="text-center nivelconhecimento">
              <li class="text-justify" >Jquery</li>
                <p class="text-left">
                    Projetos acadêmicos para estudo de responsividade.
                </p>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="3"
                    aria-valuemin="0" aria-valuemax="5" style="width:30%">
                      3
                    </div>
                  </div> 
            </div>
            
            <div class="text-center nivelconhecimento">
              <li class="text-justify" >Bootstrap</li>
                <p class="text-left">
                    Projetos acadêmicos para estudo de responsividade.
                </p>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="3"
                    aria-valuemin="0" aria-valuemax="5" style="width:30%">
                      3
                    </div>
                  </div> 
            </div>

            <div class="text-center nivelconhecimento">
              <li class="text-justify" >PHP</li>
                <p class="text-left">
                    Projetos acadêmicos para estudo de Estrutura de Dados, OO e CRUD.
                </p>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="3"
                    aria-valuemin="0" aria-valuemax="5" style="width:30%">
                      3
                    </div>
                  </div> 
            </div>

            <div class="text-center nivelconhecimento">
              <li class="text-justify" >Laravel</li>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="00"
                    aria-valuemin="0" aria-valuemax="5" style="width:00%">
                      00
                    </div>
                  </div> 
            </div>
            
            <div class="text-center nivelconhecimento">
              <li class="text-justify" >MySQL</li>
                <p class="text-left">
                    Projetos acadêmicos para estudo de DDL com foco maior em SQL Server.
                </p>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="3"
                    aria-valuemin="0" aria-valuemax="5" style="width:30%">
                      3
                    </div>
                  </div> 
            </div>

            <div class="text-center nivelconhecimento">
              <li class="text-justify" >AWS</li>
                <p class="text-left">
                    Conhecimento básico em Azure.
                </p>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="0"
                    aria-valuemin="0" aria-valuemax="5" style="width:0%">
                      00
                    </div>
                  </div> 
            </div>

            <div class="text-center nivelconhecimento">
                <li class="text-justify" >TDD</li>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="0"
                    aria-valuemin="0" aria-valuemax="5" style="width:0">
                      0
                    </div>
                  </div> 
            </div>

            <div class="text-center nivelconhecimento">
                <li class="text-justify" >ElasticSearch</li>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="0"
                    aria-valuemin="0" aria-valuemax="5" style="width:0">
                      0
                    </div>
                  </div> 
            </div>

            <div class="text-center nivelconhecimento">
              <li class="text-justify" >Desenvolvimento Mobile</li>
                <p class="text-left">
                    Desenvolvimento de Quiz em Java, utilizando Android Studio.
                </p>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="10"
                    aria-valuemin="0" aria-valuemax="5" style="width:10">
                      1
                    </div>
                  </div> 
            </div>
            
            <h1 class="text-center display-4">PARTE 1 CONCEITUAL</h1>
            <p class="text-justify perguntaquestionario">1) Você recomendaria o uso de frameworks par projetos PHP? Possui conhecimento em algum?</p>
            <p class="text-justify respostaquestionario"> Resposta: Sim recomendo, não possuo experiencia</p>
            <p class="text-justify perguntaquestionario">2) Qual as diferenças entre os seguintes tipos de métodos: Private, Public, Protected?</p>
            <p class="text-justify respostaquestionario">Resposta: Tratam de modificadores de acesso.<br> Public permite acesso por qualquer classe, método ou atributo.<br>Protected permite acesso da classe, método ou atributo que o declara<br>Private permite que somente a classe que o declarou tenha acesso, não há herança no Private</p>
            <p class="text-justify perguntaquestionario">3) Qual a diferença entre uma abstract class e uma interface?</p>
            <p class="text-justify respostaquestionario">Resposta: </p>
            
            <div id="partedois" class="text-center nivelconhecimento">
              <h1 class="text-center display-4">PARTE 2 TÉCNICA</h1>
              <p class="text-justify perguntaquestionario">1) Escreva um código PHP que receba dados de uma URL</p>
              <code>SELECT * FROM TABLENAME;</code>
              <br><br>
              <p class="text-justify perguntaquestionario">3) Conforme entidades definidas abaixo, de fornecedor e produto
              -Fornecedor (Codigo, Nome, Estado) - Produto (Codigo, Descricao, Preco, Desconto, CodFornecedor)
              </p>
              <p class="text-justify perguntaquestionario">A) Como crio as tabelas (DDL)? </p>
              <code>CREATE TABLE fornecedor (<br>
                  codigo INT NOT NULL AUTO_INCREMENT,<br>
                  nome VARCHAR( 50 ) NOT NULL,<br>
                  estado VARCHAR( 50 ) NOT NULL<br>
                  );<br>
                  <br>
                  CREATE TABLE produto (<br>
                  codigoproduto INT NOT NULL AUTO_INCREMENT,<br>
                  descricao VARCHAR( 100 ) NOT NULL,<br>
                  preco VARCHAR( 100 ) NOT NULL,<br>
                  desconto VARCHAR( 100 ) NOT NULL,<br>
                  );<br>

                  ALTER TABLE `produto` ADD CONSTRAINT `FK_CodFornecedor` FOREIGN KEY ( `codigofornececedor` ) REFERENCES `fornecedor` ( `codigo` ) ;<br>
                </code>
                <br><br>
                <p class="text-justify perguntaquestionario">B) Qual o valor de desconto médio aos fornecedores do RJ? </p>
                <code>
                  
                </code>
          
              </div>

          </div>
      
      </div>
      

      <footer class="mastfoot mt-auto">
        
      </footer>
    </div>

  </body>
</html>