<?php
include_once '../model/conexao.class.php';
include_once '../model/manager.class.php';

$manager = new Manager();

?>

<html lang="pt">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Teste PHP</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.1/examples/cover/">

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/estilo.css" rel="stylesheet">
  </head>

  <body class="text-center">

    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
    <?php
	      include_once('../menu.php');
    ?>
        <div class="container">
	
	<h2 class="text-center">
        Área de controle do administrador
	</h2>



	<div class="table-responsive">
		<table class="table table-hover">
			<thead class="thead">
				<tr>
					<th>ID</th>
					<th>NOME</th>
					<th>E-MAIL</th>
					<th>TELEFONE</th>
					<th colspan="3">AÇÕES</th>
				</tr>
			</thead>
            
            <tbody>
				<?php foreach($manager->listarDados("contato") as $client): ?>
				<tr>
					<td><?php echo $client['codigo']; ?></td>
					<td><?php echo $client['nome']; ?></td>
					<td><?php echo $client['email']; ?></td>
					<td><?php echo $client['celular']; ?></td>
					<td>
						<form method="POST" action="../view/paginaeditar.php">
							
						<input type="hidden" name="codigo" value="<?=$client['codigo']?>">

							<button class="btn btn-warning btn-xs">
								<i class="fa fa-user-edit"></i>
							</button>

						</form>
					</td>
					<td>
						<form method="POST" action="../controller/excluircontato.php" onclick="return confirm('Você tem certeza que deseja excluir ?');">
												
                            <input type="hidden" name="codigo" value="<?=$client['codigo']?>">

							<button class="btn btn-danger btn-xs">
								<i class="fa fa-trash"></i>
							</button>

						</form>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>

</div>
      

      <footer class="mastfoot mt-auto">
        
      </footer>
    </div>

    <script src="js/script.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
 
    <script type="text/javascript">
	$(document).ready(function(){
		$("#celular").mask("(00) 00000-0000");
	});
</script>
  </body>
</html>